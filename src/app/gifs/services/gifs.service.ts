import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGifsResponse } from '../interfaces/gifs.interface';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  constructor(private httpClient: HttpClient) {
    
    this._historial = JSON.parse(localStorage.getItem('historial')!) || [];
    this.resultados = JSON.parse(localStorage.getItem('resultados')!) || [];

  }

  private apiKey: string = 'A1JuBen8FxC0Ew5a5w7kExI7NKwj7Tln';
  private apiUrl: string = 'https://api.giphy.com/v1/gifs';
  private _historial: string[] = [];
  
  public resultados: Gif[] = [];

  get historial() {
    return [...this._historial];
  }

  buscarGifs( termino: string, limite?: string) {

    termino = termino.trim().toLowerCase();

    if( !this._historial.includes( termino )) {
      this._historial.unshift( termino );
      this._historial = this._historial.splice(0, 10);

      localStorage.setItem('historial', JSON.stringify(this._historial));
    }

    const params = new HttpParams()
      .set('api_key',this.apiKey)
      .set('q', termino)
      .set('limit', limite)

    this.httpClient.get<SearchGifsResponse>(`${ this.apiUrl }/search`, { params })
      .subscribe( (resp) => {
        // console.log(resp.data);
        this.resultados = resp.data;
        localStorage.setItem('resultados', JSON.stringify(this.resultados));
      })

  }

}
